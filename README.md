# Wintermute.dev

To get started, simply clone this repo and run `vagrant up`
You can tweak the configuration options inside the `Vagrantfile` to increase/decrease (the amount of ram and vcpus)

Important note: you will need to have ansible installed on your host system to provision the box.
Important note 2: After installing and provisioning the system, you can install an omf theme, preferably the one called boxfish by simply running the following command: 

```fish 
omf install boxfish
```

Important note 3: You will also have to install whatever lsp language server you require for your languages of choice. This is done by calling the `:LspInstallInfo` from within vim.

