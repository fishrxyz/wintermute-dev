---
- hosts: all
  become: true
  vars_files:
    - "./vars/global.yml"
  tasks:
    - name: Create necessary go directories
      file:
        path: "{{ item }}"
        state: directory
      with_items:
        - /home/vagrant/gowork
        - /home/vagrant/gowork/src
        - /home/vagrant/gowork/bin
        - /home/vagrant/gowork/pkg
      become: true
      become_user: vagrant

    - name: create ~/.local x newsboat dirs
      file:
        path: "{{ item }}"
        state: directory
      with_items:
        - /home/vagrant/.local
        - /home/vagrant/.local/bin
        - /home/vagrant/.local/share
        - /home/vagrant/.newsboat
      become: true
      become_user: vagrant

    - name: Copy file with owner and permission, using symbolic representation
      ansible.builtin.copy:
        src: ./files/urls
        dest: ~/.newsboat/urls
        owner: vagrant
        group: vagrant
        mode: u=rw,g=r,o=r
      become: true
      become_user: vagrant

    - name: Generate ssh keypair {{ssh_key_filename}}
      openssh_keypair:
        path: "~/.ssh/{{ssh_key_filename}}"
        type: ed25519
        size: 4096
        state: present
        force: no
      become: true
      become_user: vagrant

    - name: Install "gitlab-cli"
      community.general.snap:
        name: glab
        channel: latest/edge

    - name: Install helm
      community.general.snap:
        name: helm
        channel: classic

    - name: Grant ssh access to gitlab-cli
      shell: sudo snap connect glab:ssh-keys
      become: true

    - name: Authenticate to gitlab.com using {{gitlab_token}}
      shell: echo "{{gitlab_token}}" > {{gitlab_token_filepath}} && glab auth login --stdin < {{gitlab_token_filepath}}
      become: true
      become_user: vagrant

    - name: Delete vagrant@wintermute ssh key
      ansible.builtin.script: ./scripts/delete_ssh_key.sh
      become: true
      become_user: vagrant
      environment:
        GITLAB_TOKEN: "{{gitlab_token}}"

    - name: Add your newly created sshkey
      shell: glab ssh-key add ~/.ssh/{{ssh_key_filename}}.pub -t {{gitlab_ssh_key_title}}
      become: true
      become_user: vagrant

    - name: Setup global git config
      shell: |
        git config --global user.name {{ git_global_username }}
        git config --global user.email {{ git_global_email }}

    - name: add newly created keys to the ssh-agent
      shell: eval $(ssh-agent -s) && ssh-add ~/.ssh/{{ssh_key_filename}}
      become: true
      become_user: vagrant

    - name: Add neovim stable repo
      ansible.builtin.apt_repository:
        repo: ppa:neovim-ppa/unstable # NOTE: may be subject to change

    - name: Add python stable repo
      ansible.builtin.apt_repository:
        repo: ppa:deadsnakes/ppa

    - name: Install necessary packages
      become: true
      apt:
        update_cache: yes
        autoclean: yes
        autoremove: yes
        pkg:
          - git
          - curl
          - cmake
          - newsboat
          - xclip
          - acl
          - w3m
          - neovim
          - lua5.3
          - python3
          - python3.8-venv
          - software-properties-common
          - ca-certificates
          - gnupg
          - lsb-release
          - nginx
          - tmux
          - fish
          - wget
          - whois
          - cargo
          - python3-pip
          - apt-transport-https
          - net-tools
          - sshpass
          - unzip
          - mlocate
          - fzf
          - fd-find
          - jq
          - kubectx # also install kubens apparently
          - bat # known as batcat

    - name: add terraform key to apt
      shell: curl -fssl https://apt.releases.hashicorp.com/gpg | apt-key add -

    - name: add terraform repo to apt
      shell: apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

    - name: Install terraform
      apt:
        update_cache: yes
        pkg:
          - terraform

    - name: Symlink batcat to bat
      file:
        src: "/usr/bin/batcat"
        dest: "/usr/bin/bat"
        state: link
      become: true

    - name: Symlink fdfind to ~/.local/bin
      shell: ln -s $(which fdfind) /home/vagrant/.local/bin/fd
      become: true

    - name: Symlink python3 to python
      shell: ln -s $(which python3) /usr/bin/python
      become: true

    - name: Install latest version of go
      ansible.builtin.shell: wget 'https://go.dev/dl/{{ go_tar }}' && tar -xvf '{{go_tar}}' && mv go /usr/local && rm -rf '{{go_tar}}'
      become: true

    - name: Install ripgrep
      ansible.builtin.script: ./scripts/ripgrep.sh
      become: true

    - name: Install nodejs deb source
      ansible.builtin.shell: curl -sL https://deb.nodesource.com/setup_16.x -o /tmp/nodesource_setup.sh && bash /tmp/nodesource_setup.sh
      become: true

    - name: Install nodejs and its dependencies
      become: true
      apt:
        update_cache: yes
        autoclean: yes
        autoremove: yes
        pkg:
          - nodejs
          - gcc
          - g++
          - make

    - name: Install exa and stylua
      ansible.builtin.shell: cargo install exa stylua
      become_user: vagrant

    - name: Install "yarn" node.js package globally.
      npm:
        name: yarn
        global: yes

    - name: Install "prettier" node.js package globally.
      npm:
        name: prettier
        global: yes

    - name: Install "eslint" node.js package globally.
      npm:
        name: eslint
        global: yes

    - name: Install multi python packages with version specifiers
      pip:
        name:
          - flake8
          - linode-cli
          - black
          - ansible # install ansible on the remote machine to have access to "ansible-config" which is required for lsp completion
          - pgcli
          - aws-sam-cli
          - pdm

    - name: Install AWS CLI
      become: true
      ansible.builtin.shell: curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "/tmp/awscliv2.zip" && unzip /tmp/awscliv2.zip && ./aws/install

    # Docker x Compose setup
    - name: Install docker engine
      ansible.builtin.shell: curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
      become: true

    # Docker x Compose setup
    - name: Setup stable repo
      ansible.builtin.shell: echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
      become: true

    - name: Install docker x compose
      become: true
      apt:
        update_cache: yes
        autoclean: yes
        autoremove: yes
        pkg:
          - docker-ce
          - docker-ce-cli
          - containerd.io
          - docker-compose-plugin

    - name: Download google cloud public signing key
      become: true
      ansible.builtin.shell: curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

    - name: Add k8s apt repository
      become: true
      ansible.builtin.shell: echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

    - name: Install kubectl
      become: true
      apt:
        update_cache: yes
        autoclean: yes
        autoremove: yes
        pkg:
          - kubectl

    - name: Clone dotfiles
      git:
        repo: https://gitlab.com/fishrxyz/dotfiles.git
        dest: "~/.dotfiles"
        clone: yes
        update: yes
      become: true
      become_user: vagrant

    - name: remove default fish config
      file:
        path: "~/.config/fish/config.fish"
        state: absent
      become: true
      become_user: vagrant

    - name: copy fishr's fish config
      shell: mkdir -p ~/.config/fish && ln -sf ~/.dotfiles/fish/config.fish ~/.config/fish/
      become: true
      become_user: vagrant

    - name: Get omf repo
      git:
        repo: https://github.com/oh-my-fish/oh-my-fish
        dest: /tmp/omf
        clone: yes
      become: true
      become_user: vagrant

    - name: Install omf
      shell: /tmp/omf/bin/install -y --offline --noninteractive
      become: true
      become_user: vagrant

    - name: Create nvim directory
      ansible.builtin.file:
        path: "~/.config/nvim"
        state: directory
        mode: "0755"
      become: true
      become_user: vagrant

    - name: set user shell to fish
      shell: chsh -s /usr/bin/fish vagrant
      become: true

    - name: setup neovim config
      file:
        src: "~/.dotfiles/nvim/init.lua"
        dest: "~/.config/nvim/init.lua"
        state: link
      become: true
      become_user: vagrant

    - name: setup neovim config (pt2)
      file:
        src: "~/.dotfiles/nvim/lua/"
        dest: "~/.config/nvim/lua"
        state: link
      become: true
      become_user: vagrant

    - name: copy tmux config
      file:
        src: "~/.dotfiles/tmux/tmux.conf"
        dest: ".tmux.conf"
        state: link
      become: true
      become_user: vagrant

    - name: copy oh-my-tmux config
      shell: cp ~/.dotfiles/tmux/tmux.conf.local ~/.tmux.conf.local && sed -e '/display-popup/ s/^#*/#/' -i ~/.tmux.conf.local
      become: true
      become_user: vagrant

    - name: Install minikube
      shell: curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb && dpkg -i minikube_latest_amd64.deb && rm -rf minikube_latest_amd64.deb
      become: true

    - name: Add user to docker group
      ansible.builtin.user:
        name: vagrant
        groups: docker
        append: true
      become: true

    - name: create fish functions directory
      shell: mkdir -p ~/.config/fish/functions/
      become: true
      become_user: vagrant

    - name: Copy fish function to add ssh-key to the agent
      ansible.builtin.copy:
        src: ./files/add_key_to_agent.fish
        dest: ~/.config/fish/functions/add_key_to_agent.fish
        owner: vagrant
        group: vagrant
        mode: u=rw,g=r,o=r
      become: true
      become_user: vagrant

    - name: add correct key to add_key_to_agent function
      shell: sed -i "s/ssh_key_filename/{{ssh_key_filename}}/" ~/.config/fish/functions/add_key_to_agent.fish
      become: true
      become_user: vagrant

    - name: add linode-cli token to fish path
      ansible.builtin.lineinfile:
        path: ~/.config/fish/config.fish
        line: set -x LINODE_CLI_TOKEN "{{linode_token}}"
        insertafter: \# Env vars
      become: true
      become_user: vagrant

    - name: call ssh-agent function upon login
      ansible.builtin.lineinfile:
        path: ~/.config/fish/config.fish
        line: add_key_to_agent
        create: yes
      become: true
      become_user: vagrant
