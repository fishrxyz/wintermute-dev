#!/usr/bin/bash
VAGRANT_KEY_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/user/keys" | jq -c '.[] | select(.title|contains("vagrant@wintermute")) | .id')

curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/user/keys/${VAGRANT_KEY_ID}"
